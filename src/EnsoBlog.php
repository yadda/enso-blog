<?php

namespace Yadda\Enso\Blog;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\Route;
use ReflectionClass;
use Yadda\Enso\Blog\Contracts\PostCollectionResource;
use Yadda\Enso\Blog\Contracts\PostResource;
use Yadda\Enso\Utilities\Helpers;

class EnsoBlog
{
    /**
     * PostResource made from the given Post.
     *
     * @param Model $model
     *
     * @return JsonResource
     */
    public function postResource(Model $model): JsonResource
    {
        $resource_class = Helpers::getConcreteClass(PostResource::class);
        return $resource_class::make($model);
    }

    /**
     * Post Resource Collection. If a Resource Collection is bound to the
     * contract, use that. Otherwise uses the PostResource to provide a default
     * anonymous resource collection.
     *
     * @param mixed $models
     *
     * @return ResourceCollection
     */
    public function postResourceCollection($models): ResourceCollection
    {
        $resource_collection_class = Helpers::getConcreteClass(PostCollectionResource::class);

        if ((new ReflectionClass($resource_collection_class))->isInstantiable()) {
            return (new $resource_collection_class($models));
        } else {
            $resource_class = Helpers::getConcreteClass(PostResource::class);
            return $resource_class::collection($models);
        }
    }

    /**
     * Add frontend routes for blog
     */
    public function routes(string $path, string $class, string $name): void
    {
        Route::get($path)
            ->uses([$class, 'index'])
            ->name($name . '.index');
        Route::get($path . '/{post}')
            ->uses([$class, 'show'])
            ->name($name . '.show');
        Route::get('json/' . $path)
            ->uses([$class, 'jsonIndex'])
            ->name($name . '.index-json');
        Route::get('json/' . $path . '/categories')
            ->uses([$class, 'jsonCategories'])
            ->name($name . '.categories-json');
    }
}
