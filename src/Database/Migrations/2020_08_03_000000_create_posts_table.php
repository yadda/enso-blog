<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('enso_posts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->char('slug', 36)->unique();
            $table->bigInteger('user_id')->nullable()->index();
            $table->bigInteger('thumbnail_id')->nullable();
            $table->boolean('featured')->default(false)->index();
            $table->timestamp('publish_at')->index()->nullable();
            $table->boolean('published')->default(false)->index()->nullable();
            $table->string('template')->default('default');
            $table->timestamps();
            $table->json('content');
        });

        Schema::create('enso_category_post', function (Blueprint $table) {
            $table->bigInteger('category_id')->index();
            $table->bigInteger('post_id')->index();
            $table->timestamps();
        });

        Schema::create('enso_post_post', function (Blueprint $table) {
            $table->bigInteger('post_id')->index();
            $table->bigInteger('related_post_id')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('enso_posts');
        Schema::dropIfExists('enso_category_post');
        Schema::dropIfExists('enso_post_post');
    }
}
