<?php

namespace Yadda\Enso\Blog\Contracts;

interface PostCollectionResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request);
}
