@if (!empty($related_posts))
  <section class="md:max-w-2xl mx-auto px-5 my-10 md:my-12">
    <h2 class="text-3 font-bold leading-none mb-5">Related Posts</h2>
    @foreach ($related_posts as $post)
      @include('enso-blog::parts.post-preview')
    @endforeach
  </section>
@endif
