<?php

namespace Yadda\Enso\Blog\Controllers;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Response;
use Yadda\Enso\Blog\Contracts\Post;
use Yadda\Enso\Blog\Contracts\PostCategory;
use Yadda\Enso\Blog\Facades\EnsoBlog;
use Yadda\Enso\Facades\EnsoMeta;

class PostController extends Controller
{
    /**
     * List blog posts
     *
     * @return void
     */
    public function index(Request $request)
    {
        if ($this->expectsJson($request)) {
            return $this->jsonIndex($request);
        }

        $index_route = $request->route()->getName();
        $category_url = route(config('enso.blog.public_route') . '.categories-json');

        return view('enso-blog::index', compact(
            'index_route',
            'category_url'
        ));
    }

    /**
     * Get JSON list of categories
     */
    public function jsonCategories(): JsonResponse
    {
        $categories = App::make(PostCategory::class)
            ->query()
            ->whereHas('posts', function ($query) {
                $query->accessibleToUser();
            })
            ->orderBy('name')
            ->paginate();

        return response()->json($categories);
    }

    /**
     * Get the number of posts to return per page in a JSON response
     *
     * @return integer
     */
    public function perPage(): int
    {
        return 15;
    }

    /**
     * Json route for getting
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function jsonIndex(Request $request): JsonResponse
    {
        $category_slug = $request->input('category');

        $posts = $this
            ->makeIndexQuery($request)
            ->with($this->indexWith())
            ->tap(function ($query) use ($category_slug) {
                return $this->indexFilterCategories($category_slug, $query);
            })
            ->tap(function ($query) {
                return $this->alterIndexQuery($query);
            })
            ->tap(function ($query) {
                return $this->indexOrder($query);
            })
            ->paginate($this->perPage());

        return $this->jsonPostsResponse($posts);
    }

    /**
     * Show a single post
     */
    public function show(Request $request, $slug)
    {
        $post = App::make(Post::class)
            ->query('post')
            ->accessibleToUser()
            ->with('categories', 'thumbnail')
            ->where('slug', $slug)
            ->orderBy('publish_at', 'DESC')
            ->firstOrFail();

        $related_posts = $post->getRelated();

        $meta = $post->getMeta();
        $meta->overrideTitle($post->title);

        EnsoMeta::use($meta);

        return view($post->getViewName(), compact(
            'post',
            'related_posts'
        ));
    }

    /**
     * Hook for altering the index query
     */
    protected function alterIndexQuery(Builder $query): Builder
    {
        return $query;
    }

    /**
     * Whether the request expects a json response
     *
     * @param mixed $request
     *
     * @return boolean
     */
    protected function expectsJson($request): bool
    {
        return $request->ajax() || $request->has('ajax-request');
    }

    /**
     * Hook for filtering index query posts by category
     */
    protected function indexFilterCategories(string $category_slug = null, Builder $query): Builder
    {
        return $query->when($category_slug, function ($query) use ($category_slug) {
            $query->whereHas('categories', function ($query) use ($category_slug) {
                $query->where('slug', $category_slug);
            });
        });
    }

    /**
     * Update an index query
     */
    protected function indexOrder(Builder $query): Builder
    {
        $query->orderBy('publish_at', 'DESC');

        return $query;
    }

    /**
     * Relationships to eager load on the index query
     */
    protected function indexWith(): array
    {
        return ['categories', 'hero', 'thumbnail', 'user'];
    }

    /**
     * Format a collection of Posts for a json response
     *
     * @param mixed $posts
     */
    protected function jsonPostsResponse($posts)
    {
        /**
         * @todo - Discuss with Team whether we can/should be using Laravel's
         * resource collection data format instead of just serializing the
         * Illuminate\Pagination\LengthAwarePaginator you get as a result of
         * running a paginated DB query
         */
        return EnsoBlog::postResourceCollection($posts)->response();

        // return Response::json(
        //     EnsoBlog::postResourceCollection($posts)->resource
        // );
    }

    /**
     * Create the index query
     */
    protected function makeIndexQuery(Request $request): Builder
    {
        return App::make(Post::class)
            ->query()
            ->accessibleToUser();
        ;
    }
}
